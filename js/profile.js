function ModalProfile() {
    var str = '<h1>Profile</h1>';
    str += '<h3>It&rsquo;s fast &amp; <i>free!</i></h3>';
    str += '<p></p>';
    str += '<form name="FormProfile" action="NONE" autocomplete="off" novalidate onsubmit="return false;">';
    str += '<table cellspacing=2 cellpadding=2 style="background-color:#f9f9f9; border:0px solid #D0D0D0; color:#002166; padding-left:10px;">';
    str += '<tr><td>Zip Code</td><td><input type="text" id="profile-zipcode" placeholder="" maxlength=10 ></td><td></td></tr>';
    str += '<tr><td>City</td><td><input type="text" id="profile-city" placeholder="" maxlength=10 ></td><td></td></tr>';
    str += '<tr><td>State</td><td><input type="text" id="profile-state" placeholder="" maxlength=10 ></td><td></td></tr>';
    str += '<tr><td colspan="3" id="ProfileMessage"</td></tr>';
    str += '</table>';
    
    str += '<input id="btnLogin" value="Update" type="submit" onClick="return SubmitProfile();" >';
    str += '</form>';
    
    var modal = document.getElementById('ModalWindow');
    var modalClose = document.getElementById('ModalWindowClose');
    var modalContents = document.getElementById('ModalContents');
    
    modalContents.innerHTML = str;

    modalClose.onclick = function() {
	modal.style.display = "none";
    }
    modal.style.display = "block";
}

function ShowWindowProfile() {
    //alert("ShowWindowProfile");
    var page = document.getElementById('slidepage_profile');
    page.style.left = '0';
}

function CloseWindow_Profile() {
    var page = document.getElementById('slidepage_profile');
    page.style.left = '-95%';
}

//-------------------------------------------------
  var globalQuestionList;
  var globalQuestionAnswers = [];
  
  function quizCreate(data) {
      if (GlobalAccountName == null) {
  	  alert("Login or register.");
	  return;
      }
      var btn = document.getElementById("quiz_button");
      btn.style.display = "none";
      var xmlhttp = new ajaxRequest();
      xmlhttp.onreadystatechange = quizCallback;
      var params = {
	  "account" : GlobalAccountName,
      };
      xmlhttp.open("POST", "http://127.0.0.1:8000/quizget", true);
      xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xmlhttp.send(JSON.stringify(params));
  }
  
  function quizSaveCallback(data) {
    if (this.readyState == 4 && this.status == 200) {
          var response = JSON.parse(this.responseText);
          var quiz_div = document.getElementById('quiz_div');
	  quiz_div.innerHTML = response;
      }
  }
  
  function quizClick(idx, q_id, a_id) {
      //alert('quizAnswer idx=' + idx + ' q_id=' + q_id + ' a_id=' + a_id);
      var resp = {"question" : q_id, "answer" : a_id};
      globalQuestionAnswers.push(resp);
      var len = globalQuestionList.length;
      if ((idx+1) < len) {
	  quizDisplayQuestion(idx+1);
      } else {
	  var quiz_div = document.getElementById('quiz_div');
	  quiz_div.innerHTML = 'Saving...';
	  var xmlhttp = new ajaxRequest();
	  xmlhttp.onreadystatechange = quizSaveCallback;
	  var params = {
	      "account" : GlobalAccountName,
	      "answers" : globalQuestionAnswers
	  };
	  xmlhttp.open("POST", "http://127.0.0.1:8000/quizsave", true);
	  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	  xmlhttp.send(JSON.stringify(params));
      }
  }
  
  function quizDisplayQuestion(idx) {
      var str = '<h1 class="question">';
      str += globalQuestionList[idx].text;
      str += '</h1>';
      var len = globalQuestionList.length;
      var percent = 100.0 * parseInt(idx+1) / parseInt(len);
      str += '<div>Question: ' + (idx+1) + ' of ' + len + '</div>';
      str += '<div id="progressBar">';
      str += '<div style="width:' + percent + '%">';
      str += '</div></div>';

      str += '<form action="NONE" onsubmit="return false;">';
      var q_id = globalQuestionList[idx].q_id
      var answers = globalQuestionList[idx].answers;
      for (var i=0; i<answers.length; i++) {
	  str += '<button type="submit" class="answer" ';
	  var a_id = answers[i].a_id;
	  str += 'onclick="quizClick(' + idx + ',' + q_id + ',' + a_id + ')">';
	  str += answers[i].text;
	  str += '</button>';
      }
      str += '</form>';
      var quiz_div = document.getElementById('quiz_div');
      quiz_div.innerHTML = str;
  }
  
  function quizCallback(data) {
    //alert("quizCallback readystate=" + this.readyState + " status=" + this.status + " text=" + this.responseText);
    if (this.readyState == 4 && this.status == 200) {
          var response = JSON.parse(this.responseText);
          globalQuestionList = response.questions;
    	  quizDisplayQuestion(0);
      }
  }
