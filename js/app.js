"use strict";

    function portletClick(i) {
        return function() {
	    //alert('portlet click ' + i);
	    window.location = Portlets[i].fullpage;
        };
	// window.location.href = ""
	// window.location.assign()
	// window.location.replace()
    }

var Portlets = [
    {
	"title": 'Home page',
	"icon": "icon-information",
	"fullpage": "/sim.html",
    }, {
	"title": 'Main App Page',
	"icon": "icon-application_home",
	"fullpage": "/app.php",
    }, {
	"title": 'User Accounts',
	"icon": "icon-user",
	"height": "160px",
	"width": "150px",
	"background_color" : "blue",
	"demoFunction": portletUserAccount,
	"fullpage": "/portlet/accounts/app.php",
    }, {
	"title": 'Project Planning',
	"icon": "icon-page",
	"height": "160px",
	"width": "180px",
	"background_color" : "ivory",
	"demoFunction": portletProjectPlanning,
	"fullpage": "/portlet/planning/app.php",
    }, {
	"title": 'Location Details',
	"icon": "icon-page_world",
	"height": "160px",
	"width": "210px",
	"background_color" : "ivory",
	"demoFunction": portletLocationDetails,
	"fullpage": "/portlet/location/app.php",
    }, {
	"title": 'Documents',
	"icon": "icon-folder",
	"height": "160px",
	"width": "150px",
	"background_color" : "orange",
	"demoFunction": portletDocuments,
	"fullpage": "/portlet/documents/app.php",
    }, {
	"title": 'Project Reports',
	"icon": "icon-report",
	"height": "160px",
	"width": "150px",
	"background_color" : "puce",
	"demoFunction": portletProjectReports,
	"fullpage": "/portlet/reports/app.php",
    }, {
	"title": 'Project Pictures',
	"icon": "icon-picture",
	"height": "160px",
	"width": "250px",
	"background_color" : "lightgreen",
	"demoFunction": portletPictures,
	"fullpage": "/portlet/pictures/app.php",
    }, {
	"title": 'Inventory',
	"icon": "icon-application_view_columns",
	"height": "160px",
	"width": "300px",
	"background_color" : "yellow",
	"demoFunction": portletInventory,
	"fullpage": "/portlet/inventory/app.php",
    }, {
	"title": 'Calendar',
	"icon": "icon-calendar",
	"height": "180px",
	"width": "175px",
	"background_color" : "ivory",
	"demoFunction": portletCalendar,
	"fullpage": "/portlet/calendar/app.php",
    }, {
	"title": 'Schedules',
	"icon": "icon-calendar",
	"height": "180px",
	"width": "175px",
	"background_color" : "ivory",
	"demoFunction": portletSchedule,
	"fullpage": "/portlet/schedule/app.php",
    }, {
	"title": 'Maintenance',
	"icon": "icon-wrench",
	"height": "180px",
	"width": "180px",
	"background_color" : "lightblue",
	"demoFunction": portletMaintenance,
	"fullpage": "/portlet/maintenance/app.php",
    },
];


function portletDocuments() {
    var docHTML = '<img src="/img/books.png" alt="books">';
    return docHTML;
}

function portletProjectReports() {
    var reportHTML = '<img src="/img/custom-reports-icon.png" alt="reports">';
    return reportHTML;
}

function portletLocationDetails() {
    var mapHTML = '<img src="/img/USAStates.png" alt="map">';
    return mapHTML;
}

function portletPictures() {
    var pixHTML = '<img src="/img/pictures.png" alt="pictures">';
    pixHTML += '<img src="/img/folder_image.png" alt="folder pictures">';
    return pixHTML;
}

function portletInventory() {
    var invHTML = '<table border="1" cellspacing="0">';
    invHTML += '<tr style="background-color:#468a43; color:white; border: 1px solid black; padding: 2px;">';
    invHTML += '<th>Project</th> ';
    invHTML += '<th>Customer</th> ';
    invHTML += '<th>Equipment</th> ';
    invHTML += '<th>Count</th> ';
    invHTML += '</tr>';
    invHTML += '<tr style="background-color:#dcdcdc;">';
    invHTML += '<td>101026</td>';
    invHTML += '<td>ABC</td>';
    invHTML += '<td>Panels</td>';
    invHTML += '<td>444</td>';
    invHTML += '</tr>';
    invHTML += '<tr style="background-color:#fcfcfc;">';
    invHTML += '<td>102054</td>';
    invHTML += '<td>DEF</td>';
    invHTML += '<td>Conduit</td>';
    invHTML += '<td>500</td>';
    invHTML += '</tr>';
    invHTML += '<tr style="background-color:#dcdcdc;">';
    invHTML += '<td>102054</td>';
    invHTML += '<td>GHI</td>';
    invHTML += '<td>Inverters</td>';
    invHTML += '<td>100</td>';
    invHTML += '</tr>';
    invHTML += '<tr style="background-color:#fcfcfc;">';
    invHTML += '<td>101062</td>';
    invHTML += '<td>JKL</td>';
    invHTML += '<td>Racks</td>';
    invHTML += '<td>300</td>';
    invHTML += '</tr>';
    invHTML += '</table>';
    return invHTML;
}

function portletMaintenance() {
    var maintHTML = '<img src="/img/maintain.jpg" alt="pictures">';
    return maintHTML;
}

function portletCalendar() {
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();

    var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');

    var this_month = new Date(year, month, 1);
    var next_month = new Date(year, month + 1, 1);

    // Find out when this month starts and ends
    var first_week_day = this_month.getDay();
    var days_in_this_month = Math.round((next_month.getTime() - this_month.getTime()) / (1000 * 60 * 60 * 24));

    var calendar_html = '<table style="text-align:center;">';
    calendar_html += '<tr><td colspan="7"><b>' + months[month] + ' ' + year + '</b></td></tr>';
    calendar_html += '<tr>';

    // Fill the first week of the month with the appropriate number of blanks
    for (var week_day = 0; week_day < first_week_day; week_day++) {
	calendar_html += '<td> </td>';
    }

    week_day = first_week_day;
    for (var day_counter = 1; day_counter <= days_in_this_month; day_counter++) {
	week_day %= 7;
	if (week_day == 0)
	    calendar_html += '</tr><tr>';

	// Do something different for the current day
	if (day == day_counter)
	    calendar_html += '<td><b>' + day_counter + '</b></td>';
	else
	    calendar_html += '<td> ' + day_counter + ' </td>';

	week_day++;
    }

    calendar_html += '</tr>';
    calendar_html += '</table>';

    return calendar_html;
}

function portletProjectPlanning() {
    var projectHTML = '<div>';
    projectHTML += '<div style="height:20px; background:#db3a27; width:32px;"><span>20%</span></div>';
    projectHTML += '<div style="height:20px; background:#5aaadb; width:72px;"><span>40%</span></div>';
    projectHTML += '<div style="height:20px; background:#f2b63c; width:96px;"><span>60%</span></div>';
    projectHTML += '<div style="height:20px; background:#85c440; width:160px;"><span>100%</span></div>';
    projectHTML += '</div>';

    return projectHTML;
}

function portletSchedule() {
    var accountHTML = '<img src="/img/schedule.jpeg" alt="schedule">';
    return accountHTML;
}

function portletUserAccount() {
    var accountHTML = '<img src="/img/people.png" alt="user accounts">';
    return accountHTML;
}

