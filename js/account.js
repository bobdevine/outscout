function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + ";" + expires + "; path=/";
}

function deleteCookie(name) {
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i=0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function printCookies(w){
    cStr = "";
    pCOOKIES = new Array();
    pCOOKIES = document.cookie.split('; ');
    for (bb = 0; bb < pCOOKIES.length; bb++){
	NmeVal  = new Array();
	NmeVal  = pCOOKIES[bb].split('=');
	if (NmeVal[0]){
	    cStr += NmeVal[0] + '=' + unescape(NmeVal[1]) + '; ';
	}
    }
    return cStr;
}

var GlobalAccountName = "Your Accountname";
var cookieName = 'OUTSCOUTACCOUNT';
//-------------------------------------------------------------------------

function ajaxRequest() {
   if (window.XMLHttpRequest) // Mozilla, Safari etc
     return new XMLHttpRequest();
   else if (window.ActiveXObject) { // older IE
      var activexmodes = ["Msxml2.XMLHTTP", "Microsoft.XMLHTTP"];
      for (var i=0; i<activexmodes.length; i++) {
	try {
	 var activex = new ActiveXObject(activexmodes[i]);
	 return activex;
	}
	catch(e){ }
      }
   }

   return null;
}
//-------------------------------------------------------------------------

function Logout() {
    deleteCookie(cookieName);
    GlobalAccountName = "";
    var ag = document.getElementById("account_greeting");
    ag.innerHTML = "Hi Guest! Login or register.</" + "p>";
}

function LoginCallback (data) {
    //alert("OnReadyState state=" + this.readyState + " status=" + this.status + " text=" + this.responseText);
    if (this.readyState == 4 && this.status == 200) {
	if (this.responseText == "OK") {
	    var account = document.forms["FormLogin"]["accountName"].value;
	    createCookie(cookieName, account, 30);
	    var modal = document.getElementById('ModalWindow');
	    modal.style.display = "none";
	    var ag = document.getElementById("account_greeting");
	    ag.innerHTML = "<p>Welcome back " + account + "</" + "p>";
	    GlobalAccountName = account;
	} else {
	    var login_message = document.getElementById('loginMessage');
	    login_message.innerHTML = "Login fail: " + this.responseText;
	}
    }
}

function LoginSubmit() {
    //alert("LoginSubmit ");
    var account = document.forms["FormLogin"]["accountName"].value;
    var password = document.forms["FormLogin"]["password"].value;

    var xmlhttp = new ajaxRequest();
    xmlhttp.onreadystatechange = LoginCallback;
    var params = {
	"account" : encodeURIComponent(account),
	"password" : encodeURIComponent(password)
    };
    xmlhttp.open("POST", "http://127.0.0.1:8000/logincheck", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(JSON.stringify(params));
}

function LoginFormat(callbackFunctionName) {
    var str = '<h1>Login</h1>';
    str += '<p></p>';
    str += '<form name="FormLogin" action="NONE" autocomplete="off" novalidate onsubmit="return false;">';
    str += '<table cellspacing=2 cellpadding=2 style="background-color:#f9f9f9; border:0px solid #D0D0D0; color:#002166; padding-left:10px;">';
    str += '<tr><td class="required">Email</td>';
    str += '<td><input type="text" id="accountName" maxlength=10></td><td id="nameMessage"></td></td></tr>';
    str += '<tr><td class="required">Password</td><td><input autocomplete="off" id="password" type="password"></td><td id="passwordMessage"</td></tr>';
    str += '<tr><td colspan="3" id="loginMessage"</td></tr>';
    str += '</table>';
    str += '<label for="user_remember_me">Remember me</label>';
    str += '<input id="user_remember_me" name="user[remember_me]" value="1" type="checkbox">';
    str += '<p></p>';
    str += '<input id="btnLogin" value="Login" type="submit" onClick="return ' + callbackFunctionName + '();" >';
    str += '</form>';
    
    return str;
}

function ModalLogin() {
    //alert("LoginModal");
    var modal = document.getElementById('ModalWindow');
    var modalClose = document.getElementById('ModalWindowClose');
    var modalContents = document.getElementById('ModalContents');

    modalContents.innerHTML = LoginFormat("LoginSubmit");
    
    modalClose.onclick = function() {
	modal.style.display = "none";
    }
    modal.style.display = "block";
}

function RegisterCallback (data) {
    //alert("RegisterCallback state=" + this.readyState + " status=" + this.status + " text=" + this.responseText);
    if (this.readyState == 4 && this.status == 200) {
	if (this.responseText == "AccountRegister OK") {
	    var account = document.forms["FormRegister"]["email"].value;
	    createCookie(cookieName, account, 30);
	    var modal = document.getElementById('ModalWindow');
	    modal.style.display = "none";
	} else {
	    var login_message = document.getElementById('RegisterMessage');
	    login_message.innerHTML = this.responseText;
	}
    }
}

function SubmitRegister() {
    //alert("SubmitRegister");
    var email = document.forms["FormRegister"]["email"].value;
    var zipcode = document.forms["FormRegister"]["zipcode"].value;
    var password = document.forms["FormRegister"]["password"].value;

    var email_message = document.getElementById("emailMessage");
    var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var zipcode_filter = /^[0-9]{5}(?:-[0-9]{4})?$/;
    var colorError = "#FFDFDF";
    if (email === "") {
	email_message.innerHTML = "Email required";
	email_message.parentNode.style.backgroundColor = colorError;
	return false;
    } else if (!email_filter.test(email)) {
	email_message.innerHTML = "Invalid email address";
	email_message.parentNode.style.backgroundColor = colorError;
	return false;
    } else {
	email_message.innerHTML = "";
	email_message.parentNode.style.backgroundColor = "transparent";
    }

    var zipcode_message = document.getElementById("zipcodeMessage");
    if (zipcode === "") {
	zipcode_message.innerHTML = "ZIPcode required";
	zipcode_message.parentNode.style.backgroundColor = colorError;
	return false;
    } else if (!zipcode_filter.test(zipcode)) {
	zipcode_message.innerHTML = "Invalid zipcode address";
	zipcode_message.parentNode.style.backgroundColor = colorError;
	return false;
    } else {
	zipcode_message.innerHTML = "";
	zipcode_message.parentNode.style.backgroundColor = "transparent";
    }

    var password_message = document.getElementById("passwordMessage");
    if (password === "") {
	password_message.innerHTML = "Password required";
	password_message.parentNode.style.backgroundColor = colorError;
	return false;
    } else {
	password_message.innerHTML = "";
	password_message.parentNode.style.backgroundColor = "transparent";
    }

    // if reach here, the inputs look okay so send to server

    var xmlhttp = new ajaxRequest();
    xmlhttp.onreadystatechange = RegisterCallback;
    var params = "email=" + encodeURIComponent(email);
    params += "&password=" + encodeURIComponent(password);
    params += "&zipcode=" + encodeURIComponent(zipcode);
    xmlhttp.open("POST", "http://127.0.0.1:8000/register", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(params);
}

function ModalRegister() {
    var str = '<h1>Register</h1>';
    str += '<h3>It&rsquo;s fast &amp; <i>free!</i></h3>';
    str += '<p></p>';
    str += '<form name="FormRegister" action="NONE" autocomplete="off" novalidate onsubmit="return false;">';
    str += '<table cellspacing=2 cellpadding=2 style="background-color:#f9f9f9; border:0px solid #D0D0D0; color:#002166; padding-left:10px;">';
    str += '<tr><td class="required">Email</td><td><input type="text" id="email" placeholder="" maxlength=10 ></td><td id="emailMessage"></td></tr>';
    str += '<tr><td class="required">ZIP Code</td><td><input type="text" id="zipcode" placeholder="" maxlength=10 ></td><td id="zipcodeMessage"></td></tr>';
    str += '<tr><td class="required">Password</td><td><input autocomplete="off" id="password" type="password"></td><td id="passwordMessage"</td></tr>';
    str += '<tr><td colspan="3" id="RegisterMessage"</td></tr>';
    str += '</table>';
    
    str += '<input id="btnLogin" value="Register" type="submit" onClick="return SubmitRegister();" >';
    str += '</form>';
    
    var modal = document.getElementById('ModalWindow');
    var modalClose = document.getElementById('ModalWindowClose');
    var modalContents = document.getElementById('ModalContents');
    
    modalContents.innerHTML = str;

    modalClose.onclick = function() {
	modal.style.display = "none";
    }
    modal.style.display = "block";
}

function ModalResetPassword() {
    var str = '<h1>Reset Password</h1>';
    str += '<p></p>';
    str += '<form name="FormResetPassword" action="NONE" autocomplete="off" novalidate>';
    str += '<table cellspacing=2 cellpadding=2 style="background-color:#f9f9f9; border:0px solid #D0D0D0; color:#002166; padding-left:10px;">';
    str += '<tr><td class="required">Email</td><td><input type="text" id="reserEmail" placeholder="Email" maxlength=10 ></td><td id="resetMessage"></td></td></tr>';
    str += '<tr><td colspan="3" id="ResetModalMessage"</td></tr>';
    str += '</table>';
    str += '<input id="btnResetPassword" value="Reset" type="submit" onClick="return SubmitResetPassword();" >';
    str += '</form>';
    
    var modal = document.getElementById('ModalWindow');
    var modalClose = document.getElementById('ModalWindowClose');
    var modalContents = document.getElementById('ModalContents');
    
    modalContents.innerHTML = str;
    
    modalClose.onclick = function() {
	modal.style.display = "none";
    }
    modal.style.display = "block";
}

function validateLoginUsername() {
    var name = document.getElementById("userName");
    var name_message = document.getElementById("nameMessage");
    //alert("validateUsername " + name.value);

    if (name.value === "") {
      name_message.innerHTML = "Name required";
      name_message.parentNode.style.backgroundColor = "#FFDFDF";
    } else {
      name_message.innerHTML = "";
      name_message.parentNode.style.backgroundColor = "transparent";
      readyName = true;
      enableLoginButton();
    }
}

function validatePassword() {
    var pass_value = document.getElementById("password").value;
    //alert("validatePassword " + pass_value);
    var pass_level = 0;
    if (pass_value.match(/[a-z]/g)) {
	pass_level++;
    }
    if (pass_value.match(/[A-Z]/g)) {
	pass_level++;
    }
    if (pass_value.match(/[0-9]/g)) {
	pass_level++;
    }
    if (pass_value.length < 5) {
	if (pass_level >= 1) { pass_level--;}
    } else if (pass_value.length >= 20) {
	pass_level++;
    }
    var msg = document.getElementById("passwordMessage");
    switch (pass_level) {
	case 0: msg.innerHTML = "Very weak"; break;
	case 1: msg.innerHTML = "Weak"; break;
	case 2: msg.innerHTML = "Okay"; break;
	case 3: msg.innerHTML = "Strong"; break;
	default: msg.innerHTML = "Very strong"; break;
    }

    var pw2 = document.forms["signupForm"]["password_confirmation"].value;
    if (pw2 && pass_value == pw2)
    {
	var password_message = document.getElementById("validatePasswordMessage");
	password_message.innerHTML = "";
	password_message.parentNode.style.backgroundColor = "transparent";
	readyPassword = true;
	enableSignUpButton();
    }
  }

function validatePasswordConfirm() {
    var password_message = document.getElementById("validatePasswordMessage");
    var pw1 = document.forms["signupForm"]["password"].value;
    var pw2 = document.forms["signupForm"]["password_confirmation"].value;
    if (pw1 != pw2 || !pw1)
    {
	password_message.innerHTML = "Passwords do not match";
	password_message.parentNode.style.backgroundColor = "#FFDFDF";
    } else {
	password_message.innerHTML = "";
	password_message.parentNode.style.backgroundColor = "transparent";
	readyPassword = true;
	enableSignUpButton();
    }
}
