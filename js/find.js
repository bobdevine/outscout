function ShowWindowFind() {
    //alert("ShowWindowFind");
    var page = document.getElementById('slidepage_find');
    page.style.left = '0';
}

function CloseWindow_Find() {
    var page = document.getElementById('slidepage_find');
    page.style.left = '105%';
}

function forceLogin() {
    alert("forceLogin");

    var modal = document.getElementById('ModalWindow');
    var modalClose = document.getElementById('ModalWindowClose');
    var modalContents = document.getElementById('ModalContents');

    modalContents.innerHTML = LoginFormat("forceLoginHandler");
    
    modalClose.onclick = function() {
	modal.style.display = "none";
    }
    modal.style.display = "block";
}

function forceLoginHandler() {
    var name = document.forms["FormLogin"]["accountName"].value;
    var password = document.forms["FormLogin"]["password"].value;
    globalName = name;
    //alert("forceLoginHandler name='" + name + "' password='" + password + "'");

    var xmlhttp = new ajaxRequest();
    xmlhttp.onreadystatechange = forceLoginCallback;
    var params = {
	"account" : encodeURIComponent(account),
	"password" : encodeURIComponent(password)
    };
    xmlhttp.open("POST", "http://127.0.0.1:8000/logincheck", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(JSON.stringify(params));
}


function forceLoginCallback (data) {
    //alert("forceLoginCallback readystate=" + this.readyState + " status=" + this.status + " text=" + this.responseText);
    if (this.readyState == 4 && this.status == 200) {
	if (this.responseText == "OK") {
	    var modal = document.getElementById('ModalWindow');
	    modal.style.display = "none";

	    createCookie(cookieName, GlobalAccountName, 30);

	    //var form = document.getElementById('dine_form');
	    //form.style.display = 'none';
	    var title = document.getElementById('dining_title');
	    title.style.display = 'none';
					
	    var processing = document.createElement('span');
	    processing.appendChild(document.createTextNode('processing ...'));
	    title.parentNode.insertBefore(processing, title);
	    fetchRestaurants();
	} else {
	    var login_message = document.getElementById('loginMessage');
	    login_message.innerHTML = "Login failed: " + this.responseText;
	}
    }
}

function findrestaurants() {
    //alert("findrestaurants");
    var cookie = readCookie(cookieName);
    if (cookie == null) {
	forceLogin();
    } else {
	CloseWindow_Find();
	GlobalAccountName = cookie;
	fetchRestaurants();
    }
    
}

//-------------------------------------------------------------------

function fetchRestaurants() {
    //alert("fetchRestaurants");
    var zip = document.getElementById('zip').value;
    var radioObj = document.forms["dine_form"]["radio"];
    var radioLength = radioObj.length;
    var mood = "ALL";
    for (var i = 0; i < radioLength; i++) {
	if (radioObj[i].checked) {
	    mood = radioObj[i].value;
	}
    }
    //alert("fetchRestaurants name='" + GlobalAccountName + "' mood='" + mood + "'");

    var specialrequests = "";
    if (document.forms["dine_form"]["Request_1"].checked == true) {
	if (specialrequests.length > 0) { specialrequests += ','; }
	specialrequests += document.forms["dine_form"]["Request_1"].value;
    }
    if (document.forms["dine_form"]["Request_2"].checked == true) {
	if (specialrequests.length > 0) { specialrequests += ','; }
	specialrequests += document.forms["dine_form"]["Request_2"].value;
    }
    if (document.forms["dine_form"]["Request_3"].checked == true) {
	if (specialrequests.length > 0) { specialrequests += ','; }
	specialrequests += document.forms["dine_form"]["Request_3"].value;
    }
    //alert("fetchRestaurants specialrequests='" + specialrequests + "'");

    var sliderval = document.getElementById('sliderval');
    //alert("fetchRestaurants distance='" + sliderval.textContent + "'");

    //var instruction = document.getElementById("RestaurantListingsInstruction");
    var instruction = document.getElementById("RestaurantListings");
    instruction.innerHTML = "Fetching...";
    instruction.style.display = "block";

    var xmlhttp = new ajaxRequest();
    xmlhttp.onreadystatechange = fetchRestaurantsCallback;
    var params = {
	"account" : encodeURIComponent(GlobalAccountName),
	"zip" : zip,
	"distance" : sliderval.textContent, //innerText
	"mood" : mood,
	"review" : GlobalStarIndex,
	"cost" : GlobalCostIndex,
	"specialrequests" : specialrequests
    };
    xmlhttp.open("POST", "http://127.0.0.1:8000/getrestaurants", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(JSON.stringify(params));
}

function fetchRestaurantsCallback (data) {
    //alert("fetchRestaurantsCallback readyState=" + this.readyState + " status=" + this.status + " responseText=" + this.responseText);
    if (this.readyState == 4 && this.status == 200) {
	//alert('forceLoginCallback: responseText=' + this.responseText);
	var response = JSON.parse(this.responseText);
	//alert('forceLoginCallback: JSON text=' + list);
	//var instruction = document.getElementById("RestaurantListingsInstruction");
	//instruction.style.display = "none";
	
	var listings = document.getElementById("RestaurantListings");
	listings.innerHTML = "";

	hdr = document.createElement("h1");
	hdr.innerText = "OutScout Recommendations";
	listings.appendChild(hdr);
	subhdr = document.createElement("h5");
	subhdr.innerText = "Restaurants within " + response.query.distance + " miles of " + response.area.postalcode + " (" + response.area.city + ", " + response.area.state_full + ")";
	listings.appendChild(subhdr);

	var list = response.restaurantList;
	if (list.length == 0) {
	    listings.appendChild(document.createElement("h3")).innerText = 'None found';
	}
	
	for (var i=0; i<list.length; i++) {
	    box = document.createElement("div");
	    box.className = "restaurantbox";

	    leftbox = document.createElement("div");
	    leftbox.className = "restaurant_leftbox";
	    var price = '';
	    var ps = list[i].pricescale;
	    if (ps >= 5) {
		price += '$$$$$';
	    } else if (ps > 4 && ps <= 5) {
		price += '$$$$';
	    } else if (ps > 3 && ps <= 4) {
		price += '$$$';
	    } else if (ps > 2 && ps <= 3) {
		price += '$$';
	    } else if (ps > 1 && ps <= 2) {
		price += '$';
	    } else if (ps > 0 && ps <= 1) {
		price += '';
	    } else {
		price += 'No data';
	    }
	    var rating = '';
	    var ps = list[i].ratingscale;
	    if (ps >= 5) {
		rating += '*****';
	    } else if (ps > 4 && ps <= 5) {
		rating += '****';
	    } else if (ps > 3 && ps <= 4) {
		rating += '***';
	    } else if (ps > 2 && ps <= 3) {
		rating += '**';
	    } else if (ps > 1 && ps <= 2) {
		rating += '*';
	    } else if (ps > 0 && ps <= 1) {
		rating += '';
	    } else {
		rating += 'Not rated';
	    }
	    var scale = "Price: " + price + '\nRating: ' + rating;
	    leftbox.appendChild(document.createElement("span")).innerText = scale;

	    mapbox = document.createElement("div");
	    mapbox.className = "restaurant_mapbox";
	    var mapbutton = document.createElement("input");
	    mapbutton.type = "button";
	    mapbutton.value = "Get Map";
	    mapbutton.onclick = function() { alert('map button') };
	    mapbox.appendChild(mapbutton);

	    midbox = document.createElement("div");
	    midbox.className = "restaurant_midbox";
	    midbox.appendChild(document.createElement("h3")).innerText = list[i].name;
	    midbox.appendChild(document.createElement("p")).innerText = list[i].address;
	    rightbox = document.createElement("div");
	    rightbox.className = "restaurant_rightbox";
	    //rightbox.appendChild(document.createElement("p")).innerText = list[i].info;
	    rightbox.appendChild(document.createElement("p")).innerText = list[i].source;	    

	    box.appendChild(leftbox);
	    box.appendChild(mapbox);
	    box.appendChild(rightbox);
	    box.appendChild(midbox);
	    listings.appendChild(box);
	}
    }
}
