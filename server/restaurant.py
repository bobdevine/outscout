import MySQLdb
import requests
import urllib
import json

# pip install yelp
#from yelp.client import Client
#from yelp.oauth1_authenticator import Oauth1Authenticator

# pip install zomato-sdk

#from pprint import pprint

class Restaurant():
    """Model a restaurant."""
    def __init__(self, restaurant_name):
        """Initialize restaurant values."""
        self.name = restaurant_name
        self.source = []
        self.price = []
        self.rating = []
        self.latlong = []
    def setAddress(self, address):
        self.address = address
    def addSource(self, source):
        if source in self.source:
            return
        self.source.append(source)
    def addPrice(self, source, price):
        #print "addPrice: source=", source, " price=", price
        tuple = {'source' : source, 'val' : price}
        self.price.append(tuple)
    def addRating(self, source, rating):
        #print "addRating: source=", source, " rating=", rating
        tuple = {'source' : source, 'val' : rating}
        self.rating.append(tuple)
    def addLatLng(self, source, lat, lng):
        tuple = {'source' : source, 'lat' : lat, 'long' : lng}
        self.latlong.append(tuple)
    def normalizePrice(self):
        # calculate weighted score
        numbers = []
        for item in self.price:
            #print "normalizePrice: source=", item['source'], " price=", item['val']
            if item['source'] == 'google':
                numbers.append(item['val'])
            elif item['source'] == 'yelp':
                numbers.append(item['val'])
            elif item['source'] == 'zomato':
                numbers.append(item['val'])
            else:
                print "normalizePrice: unhandled source", source
        if len(numbers) > 0:
            return sum(numbers) / len(numbers)
        else:
            return 0
    def normalizeRating(self):
        numbers = []
        for item in self.rating:
            #print "normalizeRating: source=", item['source'], " rating=", item['val']
            if item['source'] == 'google':
                numbers.append(item['val'])
            elif item['source'] == 'yelp':
                numbers.append(item['val'])
            elif item['source'] == 'zomato':
                numbers.append(item['val'])
            else:
                print "normalizeRating: unhandled source", item['source']
        if len(numbers) > 0:
            return sum(numbers) / len(numbers)
        else:
            return 0


class RestaurantList():
    """ Get list of restaurants."""
    def __init__(self, params, db):
        self.account = params['account']
        self.mood = params['mood']
        self.review = params['review']
        self.cost = params['cost']
        self.specialrequests = params['specialrequests']
        self.distance = params['distance']
        self.zipcode = params['zip']
        #print "account", self.account
        #print "mood", self.mood 
        #print "review", self.review
        #print "cost", self.cost
        #print "specialrequests", self.specialrequests
        #print "distance", self.distance
        #print "GETTING RESTAURANTS FOR ZIPCODE", self.zipcode

        self.latitude = 0
        self.longitude = 0
        self.city = ''
        self.state = ''
        self.state_full = ''
        self.country = 'USA'
        try:
            cur = db.cursor()
            sql = """SELECT latitude,longitude,city,state,full_state FROM zip_codes WHERE zip ='%s'""" % self.zipcode
            #print "lat/lon lookup sql", sql
            cur.execute(sql)
            if cur.rowcount == 1:
                row = cur.fetchone()
                lat = row[0].strip()
                lat = "{0:.2f}".format(float(lat))
                lon = row[1].strip()
                lon = "{0:.2f}".format(float(lon))
                self.latitude = str(lat)
                self.longitude = str(lon)
                self.city = row[2]
                self.state = row[3]
                self.state_full = row[4]
                #print "latitude", self.latitude
                #print "longitude", self.longitude
                #print "city", self.city
                #print "state", self.state
                #print "state_full", self.state_full
        except MySQLdb.Error as e:
            print "Lat/lon database error [%s]" % str(e)
                    

    def findAllMatchingRestaurants(self):
        #print "findAllMatchingRestaurants"
        listRaw = []
        getFoursquare(listRaw, self.latitude, self.longitude)
        getFactual(listRaw, self.latitude, self.longitude)
        getGoogle(listRaw, self.latitude, self.longitude)
        getTripAdvisor(listRaw, self.latitude, self.longitude)
        getYelp(listRaw, self.latitude, self.longitude)
        getZomato(listRaw, self.latitude, self.longitude)

        listReturn = []
        for rest in listRaw:
            #print rest
            place = {}
            place['name'] = rest.name
            place['address'] = rest.address
            if len(rest.source) == 1:
                place['source'] = 'Source: ' + rest.source[0]
            else:
                place['source'] = 'Sources: ' + ', '.join(rest.source)
            place['info'] = 'zzz'
            place['pricescale'] = rest.normalizePrice()
            place['ratingscale'] = rest.normalizeRating()
            listReturn.append(place)
        return listReturn
        
    def buildResponse(self, listRaw):
        response = {
            "query": {
                "account": self.account,
                "mood": self.mood ,
                "review": self.review,
                "cost": self.cost,
                "specialrequests": self.specialrequests,
                "distance": self.distance,
            },
            "area": {
                "latitude" : self.latitude,
                "longitude" : self.longitude,
                "city" : self.city,
                "state" : self.state,
                "state_full" : self.state_full,
                "country" : self.country,
                "postalcode" : self.zipcode,
            },
            "restaurantList" : listRaw
        }
        return response


def getGoogle(restaurantList, lat, lng):
    # https://developers.google.com/places/web-service/search#PlaceSearchRequests
    googleKey = 'AIzaSyDVcXtzAfaSP2iD78BdfR5X-E_cTLm2RtM'
    radius = 2000

    url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%s,%s&radius=%s&type=restaurant&key=%s" % (lat, lng, radius, googleKey)
    #print url
    response = urllib.urlopen(url)
    jsonRaw = response.read()
    jsonData = json.loads(jsonRaw)
    #print "RESPONSE:", jsonData

    #if googleResponse.status_code != 200:
    #    print('Status:', googleResponse.status_code, 'Problem with the request.')
    #    return

    results = jsonData['results']
    for place in results:
        #print "GOOGLE PLACE: ", place
        #print "GOOGLE PLACE NAME:", place['name']
        found = False
        for rest in restaurantList:
            if place['name'] == rest.name:
                found = True
                break
        # TODO: check for other duplicates
        if found == False:
            rest = Restaurant(place['name'])

        rest.addSource('Google')
        if 'geometry' in place:
            rest.addLatLng('google',\
                           place['geometry']['location']['lat'],\
                           place['geometry']['location']['lng'])
        if 'rating' in place:
            rest.addRating('google', float(place['rating']))
        if 'price_level' in place:
            rest.addPrice('google', float(place['price_level']))

        if found == False:
            if 'vicinity' in place:
                rest.setAddress(place['vicinity'])
            restaurantList.append(rest)


def getFactual(restaurantList, lat, lng):
    # beta for API ends September 2017
    pass
    
def getFoursquare(restaurantList, lat, lng):
    CLIENT_ID = "3450UTPFH1ILSZI1XTNXUTYTTA3EL0EUR0ZFUDFPUCBCKB2E"
    CLIENT_SECRET = "1L3IWDMJGGTBBDTUKHO1VUPPAC2IJ4DESMGGIJLLA0PKRNKH"

    ll = "%s,%s" % (lat, lng)
    url = "https://api.foursquare.com/v2/venues/search?v=20161016&ll=%s8&query=restaurant&intent=checkin&client_id=%s&client_secret=%s"  % (ll, CLIENT_ID, CLIENT_SECRET)
    #print url
    response = urllib.urlopen(url)
    jsonRaw = response.read()
    jsonData = json.loads(jsonRaw)
    #print "RESPONSE:", jsonData

    results = jsonData['response']['venues']
    for place in results:
        #print "FOURSQUARE PLACE: ", place
        #print "FOURSQUARE PLACE NAME:", place['name']
        found = False
        for rest in restaurantList:
            if place['name'] == rest.name:
                found = True
                break
        # TODO: check for other duplicates
        if found == False:
            rest = Restaurant(place['name'])

        rest.addSource('Foursquare')
        if 'labeledLatLngs' in place:
            rest.addLatLng('foursquare',\
                           place['labeledLatLngs']['lat'],\
                           place['labeledLatLngs']['lng'])

        if found == False:
            if 'location' in place:
                rest.setAddress(place['location']['formattedAddress'])
            restaurantList.append(rest)


def getTripAdvisor(restaurantList, lat, lng):
    #TripAdvisor prohibits mixing with others
    pass
    

def getYelp(restaurantList, lat, lng):
    yelpClientID = '3rpccmjV0vFwekiypM9sog' 
    yelpClientSecret = 'qI3F6otOMpVYKtpjNQBuj0hhE3AzVGEo94I1YbOYWStT9i1crveCi9ZjZaqPIg7s'
    data = {'grant_type': 'client_credentials',
            'client_id': yelpClientID,
            'client_secret': yelpClientSecret}
    token = requests.post('https://api.yelp.com/oauth2/token', data = data)
    access_token = token.json()['access_token']
    
    url = 'https://api.yelp.com/v3/businesses/search'
    headers = {
        'Authorization': 'bearer %s' % access_token,
        'content-type': 'application/x-www-form-urlencoded'
    }
    #yelpResponse = requests.get(url = url, headers = headers)
    #response_data = yelpResponse.json()
    #print response_data
    url_params = {
        'term': 'restaurant',
        'latitude' : lat,
        'longitude' : lng
    }
    response = requests.request('GET', url, headers=headers, params=url_params)
    #pprint.pprint(response, indent=2)
    response_data = response.json()
    #print "RESPONSE_DATA:", response_data
    
    businesses = response_data['businesses']
    for place in businesses:
        #print "YELP PLACE: ", place
        #print "YELP PLACE NAME:", place['name']
        found = False
        for rest in restaurantList:
            if place['name'] == rest.name:
                found = True
                break
        if found == False:
            rest = Restaurant(place['name'])

        rest.addSource('Yelp')
        if 'coordinates' in place:
            rest.addLatLng('yelp',\
                           place['coordinates']['latitude'],\
                           place['coordinates']['longitude'])
        if 'rating' in place:
            rest.addRating('yelp', float(place['rating']))
        if 'price' in place:
            rest.addPrice('yelp', len(place['price']))

        if found == False:
            if 'location' in place:
                rest.setAddress(place['location']['display_address'])
            restaurantList.append(rest)


def getZomato(restaurantList, lat, lng):
    # https://developers.zomato.com/documentation#!/restaurant/search
    APIkey = "0c5efb20edc1eabfb5465053ebcc8b85"

    #https://developers.zomato.com/api/v2.1/search?entity_id=1&entity_type=city&start=20&count=20 gives you restaurants from 21 to 40

    url = "https://developers.zomato.com/api/v2.1/geocode?lat=%s&lon=%s" % (lat, lng)
    header = {"User-agent": "curl/7.43.0",
              "Accept": "application/json",
              "user_key": APIkey}

    #print url
    response = urllib.urlopen(url)
    response = requests.get(url, headers=header)
    jsonData = response.json()
    #print "ZOMATO RESPONSE:", jsonData

    if 'nearby_restaurants' in jsonData:
        results = jsonData['nearby_restaurants']
    else:
        print "ZOMATO RESPONSE (no nearby_restaurants):", jsonData
        return
    
    for place in results:
        #print "ZOMATO PLACE: ", place
        place = place['restaurant']
        #print "ZOMATO PLACE NAME:", place['name']
        found = False
        for rest in restaurantList:
            if place['name'] == rest.name:
                found = True
                break
        # TODO: check for other duplicates
        if found == False:
            rest = Restaurant(place['name'])

        rest.addSource('Zomato')
        if 'location' in place:
            rest.addLatLng('zomato',\
                           place['location']['latitude'],\
                           place['location']['longitude'])
        if 'user_rating' in place:
            rest.addRating('zomato', float(place['user_rating']['aggregate_rating']))
        if 'price_range' in place:
            rest.addPrice('zomato', float(place['price_range']))

        if found == False:
            if 'location' in place:
                #print "ZOMATO:", place['name'], place['location']['address']
                rest.setAddress(place['location']['address'])
            restaurantList.append(rest)
