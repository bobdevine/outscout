class Profile(object):
    """
    # Big Five personality traits
    Neuroticism - A tendency to easily experience unpleasant emotions such as anxiety, anger, or depression.
    Extroversion - Energy, surgency, and the tendency to seek stimulation and the company of others.
    Agreeableness - A tendency to be compassionate and cooperative rather than suspicious and antagonistic towards others.
    Conscientiousness - A tendency to show self-discipline, act dutifully, and aim for achievement.
    Openness to experience - Appreciation for art, emotion, adventure, and unusual ideas; imaginative and curious.

    # HEXACO = 6 dimensions
     Honesty-Humility
     Emotionality
     eXtraversion
     Agreeableness (versus Anger)
     Conscientiousness
     Openness to Experience

    # Restaurant dimentions
    Cleanliness
    High quality food
    Good service
    Uniqueness
    Aesthetics
    Branding
    Safe equipment
    Complex menu
    On time delivery of food
    """
    def __init__(self):
        pass
    
class RecommendationEngine(object):
    """
    Minimal interface to be implemented by recommenders, along with
    some helper methods. A concrete recommender must implement the
    recommend_items() method and should provide its own implementation
    of __str__() so that it can be identified when printing results.
    """
    def __init__(self):
        pass
    def calculateCompatability(self, person, restaurant):
        return 0.0
    def Personalize(self, account, listRaw, db):
        return listRaw

def Similarity(a, b):
    # cosine similarity calculation

    # sum of pair-wise multiplications
    numerator = 0
    # a[0] * b[0] + a[1] * b[1] + ...  a[n] * b[n]

    denominator = 1
    # multiplication of square-root of summed squares
    # root( square(a[0]) + square(a[1]) + ... square(a[n]))
    # times
    # root( square(b[0]) + square(b[1]) + ... square(b[n]))
    return numerator / denominator

def bayesian(R, v, m, C):
    """
    Computes the Bayesian average for the given parameters

    :param R: Average rating for this business
    :param v: Number of ratings for this business
    :param m: Minimum ratings required
    :param C: Mean rating across the entire list
    :returns: Bayesian average
    """

    # Convert to floating point numbers
    R = float(R)
    v = float(v)
    m = float(m)
    C = float(C)

    return ((v / (v + m)) * R + (m / (v + m)) * C)

