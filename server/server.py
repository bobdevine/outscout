import sys
import BaseHTTPServer
import MySQLdb
import json

from quiz import QuizMake, QuizScore
from restaurant import RestaurantList
from recommend import RecommendationEngine

class MyHandlerClass(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self):
        #try:
        #command,args = self.path.split("?", 2)
        message_parts = [
            'CLIENT VALUES:',
            '<br> client_address=%s (%s)' % (self.client_address,
                                             self.address_string()),
            '<br> command=%s' % self.command,
            '<br> path=%s' % self.path,
            '<br> request_version=%s' % self.request_version,
            '<br>',
            '<br>SERVER VALUES:',
            '<br> server_version=%s' % self.server_version,
            '<br> sys_version=%s' % self.sys_version,
            '<br> protocol_version=%s' % self.protocol_version,
            '<br>',
            '<br>HEADERS RECEIVED:',
        ]
        for name, value in self.headers.items():
            message_parts.append('<br> %s=%s' % (name, value.rstrip()))
            message = '\r\n'.join(message_parts)
                
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.send_header('Content-Length', len(message))
        # switch to CORS and JSONP...
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Connection', 'close')
        self.end_headers()
        
        self.wfile.write(message)
        if not self.wfile.closed:
            self.wfile.flush()
        self.wfile.close()
        self.rfile.close()
        #except :
            #e = sys.exc_info()[0]
            #self.send_error(400, "<p>Error: %s</p>" % e)

    def do_POST(self):
        #print "\n====================="
        #print "self.path", self.path
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        #print "post_body=", post_body
        try:
            db = MySQLdb.connect(user='outscout', passwd='outscout',
                                 host='127.0.0.1', db='outscout')
        except MySQLdb.Error as e:
            print "Database connection error [%s]" % str(e)
            
        if self.path == '/logincheck':
            message = LoginCheck(post_body, db)
        elif self.path == '/zipcheck':
            message = ZipCheck(post_body, db)
        elif self.path == '/register':
            message = AccountRegister(post_body, db)
        elif self.path == '/quizget':
            message = QuizGet(post_body, db)
        elif self.path == '/quizsave':
            message = QuizSave(post_body, db)
        elif self.path == '/getrestaurants':
            message = GetRestaurants(post_body, db)
        else:
            message = self.raw_requestline

        db.close()

        #try:
        self.send_response(200)
        self.send_header('Content-type', "text/plain")
        self.send_header('Content-Length', len(message))
        self.send_header('Connection', 'close')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        self.wfile.write(message)
        if not self.wfile.closed:
            self.wfile.flush()
        self.wfile.close()
        self.rfile.close()
        #except:

def LoginCheck(args, db):
    params = json.loads(args)
    account = params['account']
    password = params['password']
    #print "account:", account
    #print "password:", password
    try:
        sql = "SELECT * FROM accounts WHERE account ='" + account + "' and password='" + password + "'"
        cur = db.cursor()
        cur.execute(sql)
        numrows = cur.rowcount
        if numrows == 1:
            return 'OK'
        else:
            return 'Bad name/password'
    except MySQLdb.Error as e:
        return 'Database error'

def AccountRegister(args, db):
    params = args.split("&")
    account = params[0].split("=")[1]
    password = params[1].split("=")[1]
    zipcode = params[2].split("=")[1]
    #print "account:", account
    #print "password:", password
    #print "zipcode:", zipcode
    sql = """INSERT INTO accounts (account, password, zipcode) VALUES('%s', '%s', '%s')""" % (account, password, zipcode)
    #print "Insert SQL", sql
    cur = db.cursor()
    cur.execute(sql)
    #print "Number of rows inserted: %d" % cur.rowcount
    numrows = cur.rowcount
    db.commit()
    if numrows == 1:
        return 'AccountRegister OK'
    else:
        return 'AccountRegister failed'


def ZipCheck(args, db):
    params = json.loads(args)
    zip = params['zip']
    #print "zip:", zip
    try:
        sql = "SELECT city,state FROM zip_codes WHERE zip ='" + zip + "'"
        cur = db.cursor()
        cur.execute(sql)
        numrows = cur.rowcount
        if numrows == 1:
            row = cur.fetchone()
            city = row[0]
            state = row[1]
            return "%s, %s" % (city, state)
        else:
            return 'Unknown Zipcode'
    except MySQLdb.Error as e:
        return 'Database error'

def QuizGet(args, db):
    params = json.loads(args)
    quiz = QuizMake()
    return json.dumps(quiz)

def QuizSave(args, db):
    params = json.loads(args)
    account = params['account']
    answers = params['answers']
    print "account:", account
    #print "answers:", answers
    scores = QuizScore(answers)
    try:
        # INSERT ... ON DUPLICATE KEY
        sql = "SELECT * FROM profiles WHERE account ='" + account + "'"
        #print sql
        cur = db.cursor()
        cur.execute(sql)
        numrows = cur.rowcount
        if numrows == 1:
            updatesql = "UPDATE profiles SET" + \
            "  o_value='" + str(scores.o) + "'" + \
            ", c_value='" + str(scores.c) + "'" + \
            ", e_value='" + str(scores.e) + "'" + \
            ", a_value='" + str(scores.a) + "'" + \
            ", n_value='" + str(scores.n) + "'" + \
            " WHERE account ='" + account + "'"
            print updatesql
            cur.execute(updatesql)
            db.commit()
            summary = "Profile updated"
        else:
            insertsql = "INSERT INTO profiles(o_value,c_value,e_value,a_value,n_value,account) VALUES" + \
            "('" + str(scores.o) + "'" + \
            ",'" + str(scores.c) + "'" + \
            ",'" + str(scores.e) + "'" + \
            ",'" + str(scores.a) + "'" + \
            ",'" + str(scores.n) + "'" + \
            ",'" + account + "')"
            print insertsql
            cur.execute(insertsql)
            db.commit()
            summary = "Profile added"
    except MySQLdb.Error as e:
        return json.dumps('Database error')
    return json.dumps(summary)

def GetRestaurants(args, db):
    params = json.loads(args)
    account = params['account']

    rl = RestaurantList(params, db)
    listRaw = rl.findAllMatchingRestaurants()

    rec = RecommendationEngine()
    listRec = rec.Personalize(account, listRaw, db)
    return json.dumps(rl.buildResponse(listRec))


#Protocol = "HTTP/1.0"
#HandlerClass.protocol_version = Protocol
def main():
    if sys.argv[1:]:
        port = int(sys.argv[1])
    else:
        port = 8000
        server_address = ('127.0.0.1', port)

    try:
        httpd = BaseHTTPServer.HTTPServer(server_address, MyHandlerClass)
        sa = httpd.socket.getsockname()
        print "Starting HTTP on", sa[0], "port", sa[1], "..."
        httpd.serve_forever()
    except KeyboardInterrupt:
        print 'KeyboardInterrupt, shutting down'
        httpd.socket.close()


if __name__ == '__main__':
    main()
