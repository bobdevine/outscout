from recommend import Profile

GlobalQuiz = {
    "version" : 1.0,
    "questions" : [
        {
            "q_id" : 0,
            "text" : "If you go on a trip...",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "You plan out where to go and what to do.",
                    "openness": 0,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "You just get in the car and go!",
                    "openness": 0,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "You first check for bargains.",
                    "openness": 0,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
            ]
        },
        {
            "q_id" : 1,
            "text" : "I am constantly sampling new and different foods:",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "Agree",
                    "openness": 5,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "Neutral",
                    "openness": 3,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "Disagree",
                    "openness": 1,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
            ]
        },
        {
            "q_id" : 2,
            "text" : "How do you rate new foods?",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "I am constantly sampling new and different foods.",
                    "openness": 5,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "If I don't know what a food is, I won't try it.",
                    "openness": 0,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "At parties, I will try new foods.",
                    "openness": 1,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 3,
                    "text" : "I like to try new ethnic restaurants.",
                    "openness": 3,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
            ]
        },
        {
            "q_id" : 3,
            "text" : "Your regular restaurant should be?",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "Undiscovered by crowds",
                    "openness": 5,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "Give me a burger and I'm happy",
                    "openness": 1,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "Close to home",
                    "openness": 2,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 3,
                    "text" : "The owner knows your name",
                    "openness": 1,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
            ]
        },
        {
            "q_id" : 4,
            "text" : "How do you eat?",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "Quick eater",
                    "openness": 0,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "Plate is organized to separate the different foods",
                    "openness": 0,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "Eat only one food at a time",
                    "openness": 0,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 4,
                    "text" : "Picky eater",
                    "openness": 0,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
            ]
        },
        {
            "q_id" : 5,
            "text" : "Your food shopping is done by:",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "You",
                    "openness": 4,
                    "conscientiousness": 5,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "Someone in your family",
                    "openness": 3,
                    "conscientiousness": 2,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "Other",
                    "openness": 1,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
            ]
        },
        {
            "q_id" : 6,
            "text" : "Have a vivid imagination?",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "Disagree",
                    "openness": 1,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "Neutral",
                    "openness": 3,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "Agree",
                    "openness": 5,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
            ]
        },
        {
            "q_id" : 7,
            "text" : "I enjoy cooking for others and myself.",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "Disagree",
                    "openness": 1,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "Neutral",
                    "openness": 3,
                    "conscientiousness": 0,
                    "extraversion": 3,
                    "agreeableness": 3,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "Agree",
                    "openness": 5,
                    "conscientiousness": 0,
                    "extraversion": 5,
                    "agreeableness": 4,
                    "neuroticism": 0,
                },
            ]
        },
        {
            "q_id" : 8,
            "text" : "I care whether or not a table is nicely set.",
            "answers" : [
                {
                    "a_id" : 0,
                    "text" : "Disagree",
                    "openness": 3,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 1,
                    "text" : "Neutral",
                    "openness": 3,
                    "conscientiousness": 0,
                    "extraversion": 0,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
                {
                    "a_id" : 2,
                    "text" : "Agree",
                    "openness": 3,
                    "conscientiousness": 5,
                    "extraversion": 4,
                    "agreeableness": 0,
                    "neuroticism": 0,
                },
            ]
        },
    ]
}

def QuizMake():
    # should return only minimum
    return GlobalQuiz

def QuizScore(answers):
    #print "QuizScore answers:", answers
    scores = Profile()

    scores.o = 8
    scores.c = 38
    scores.e = 14
    scores.a = 14
    scores.n = 20

    for elem in answers:
        #print elem['question'], elem['answer']
        q = int(elem['question'])
        a = int(elem['answer'])

        question = GlobalQuiz['questions'][q]
        choice = question['answers'][a]
        scores.o += choice['openness']
        scores.c += choice['conscientiousness']
        scores.e += choice['extraversion']
        scores.a += choice['agreeableness']
        scores.n += choice['neuroticism']

    return scores
        
