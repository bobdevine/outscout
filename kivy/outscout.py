from kivy.app import App

import time
#from kivy.clock import Clock

from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
#from kivy.uix.stacklayout import StackLayout
#from kivy.uix.floatlayout import FloatLayout
from kivy.uix.anchorlayout import AnchorLayout
#from kivy.uix.scrollview import ScrollView
from kivy.uix.popup import Popup
from kivy.uix.checkbox import CheckBox

from kivy.properties import ObjectProperty

from kivy.uix.listview import SelectableView, CompositeListItem, ListItemReprMixin
from kivy.adapters.models import SelectableDataItem
from kivy.adapters.listadapter import ListAdapter

from kivy.uix.screenmanager import ScreenManager, Screen

from kivy.config import Config
from kivy.core.window import Window

from kivy.storage.jsonstore import JsonStore

import requests
import json

from kivy.lang import Builder
Builder.load_string("""
<LoginPopup>:
    title: "OutScout Login"
    size_hint: None,None
    #size_hint: None, 0.9
    orientation: 'vertical'
    padding: [10,50,10,50]
    spacing: 50
    width: 300
    height: 300
    #width : max(0.95 * Window.width, dp(500))
    auto_dismiss: False
    messageLabel : messageLabel
    txt_account : input_account
    txt_password : input_password
    GridLayout:
        cols:2
        Label: 
            size_hint: None,None
            height: 40
            text: "Email"
        TextInput:
            id: input_account
            size_hint: None,None
            hint_text: "Your email"
            on_text_validate: root.validate()
            multiline: False
            height: 40
            width: 150
        Label: 
            size_hint: None,None
            height: 40
            text: "Password"
        TextInput:
            id: input_password
            multiline: False
            password: True
            hint_text: "Your password"
            size_hint: None,None
            height: 40
            width: 150
        Label:
            size_hint: None,None
            height: 40
            text: ""
        Label: 
            id: messageLabel
            size_hint: None,None
            height: 40
            text: ""
        Button:
            text: "Login"
            size_hint: None,None
            height: 40
            on_press: root.do_login(root, root.txt_account.text, root.txt_password.text)

<ScreenMain>:
    name: 'OutScout'
    BoxLayout:
        pos_hint: {'center_x': .5}
        orientation: 'vertical'
        align: 'center'
        Image:
            source: '../img/outscout_logo.png'
            size: (400, 400)
        Label:
            width: sp(200)
            height: sp(30)
            font_size: 60
            text: "Discover great restaurants"
        Label:
            width: sp(200)
            height: sp(30)
            halign: 'center'
            text: root.currentAppDateTime()
        Label:
            width: sp(200)
            height: sp(30)
            text: 'Our AI/recommendation engine helps you discover new places.'
        Button:
            size_hint_y: None
            height: 40
            background_color: 0.18, .5, .92, 1
            text: 'Find Restaurants -->'
            on_press: app.gotoScreenFilter()

<StarButton@ToggleButton>:
    size_hint: None,None
    group: 'rating'
    size: self.texture_size

<DollarButton@ToggleButton>:
    size_hint: None,None
    group: 'expense'
    size: self.texture_size

<ScreenFilter>:
    size_hint: 1,1
    distance: slider_distance
    resturantMood: 
    GridLayout:
        padding: 10
        spacing: 10
        cols: 1
        AnchorLayout:
            anchor_x: 'center'
            anchor_y: 'top'
            Button:
                size_hint: None,None
                height: 40
                background_color: .2, .7, .5, 1
                text: '<-- Main'
                on_press: app.gotoScreenMain()
        GridLayout:
            cols: 1
            Label:
                text: "Restaurant Filter"
                font_size: '50sp'
                halign: 'left'
        GridLayout:
            cols: 2
            Label:
                text: "Distance (%d miles)" % int(slider_distance.value)
            Slider:
                id: slider_distance
                max: 100
                on_value: slider_distance.value = self.value
        GridLayout:
            cols: 2
            Label:
                text: "Dining Mood"
            GridLayout:
                cols: 2
                CheckBox:
                    size_hint_x: None
                    width: '32sp'
                    group: "mood"
                    state: "down"
                    on_active: root.cbSetMood('All', self.active)
                Label:
                    text: "All"
                    size_hint_x: None
                    size: self.texture_size
                CheckBox:
                    size_hint_x: None
                    width: '32sp'
                    group: "mood"
                    on_active: root.cbSetMood('Formal', self.active)
                Label:
                    text: "Formal"
                    size_hint_x: None
                    size: self.texture_size
                CheckBox:
                    size_hint_x: None
                    width: '32sp'
                    group: "mood"
                    on_active: root.cbSetMood('Casual', self.active)
                Label:
                    text: "Casual"
                    size_hint_x: None
                    size: self.texture_size
                CheckBox:
                    size_hint_x: None
                    width: '32sp'
                    group: "mood"
                    on_active: root.cbSetMood('Romantic', self.active)
                Label:
                    text: "Romantic"
                    size_hint_x: None
                    size: self.texture_size
        GridLayout:
            cols: 2
            Label:
                text: "Facilities"
            GridLayout:
                cols: 2
                CheckBox:
                    size_hint_x: None
                    width: '32sp'
                    on_active: root.cbSetFacilities('Party', self.active)
                Label:
                    text: "Party/Group Seating"
                    size_hint_x: None
                    size: self.texture_size
                CheckBox:
                    size_hint_x: None
                    width: '32sp'
                    on_active: root.cbSetFacilities('Handicap', self.active)
                Label:
                    text: "Wheelchair Access"
                    size_hint_x: None
                    size: self.texture_size
                CheckBox:
                    size_hint_x: None
                    width: '32sp'
                    on_active: root.cbSetFacilities('Patio', self.active)
                Label:
                    text: "Patio"
                    size_hint_x: None
                    size: self.texture_size
        GridLayout:
            cols: 2
            Label:
                text: "Rating"
            GridLayout:
                cols: 5
                padding: 20
                spacing: 20
                StarButton:
                    text: '*'
                StarButton:
                    text: '**'
                StarButton:
                    text: '***'
                StarButton:
                    text: '****'
                StarButton:
                    text: '*****'
        GridLayout:
            cols: 2
            Label:
                text: "Prices"
            GridLayout:
                cols: 5
                padding: 20
                spacing: 20
                DollarButton:
                    text: '$'
                DollarButton:
                    text: '$$'
                DollarButton:
                    text: '$$$'
                DollarButton:
                    text: '$$$$'
                DollarButton:
                    text: '$$$$$'
        AnchorLayout:
            anchor_x: 'center'
            anchor_y: 'bottom'
            Button:
                size_hint: None,None
                height: 40
                text: 'Listing -->'
                background_color: .2, .7, .7, 1
                on_press: root.gotoScreenListing()
 
<CIButton>:
    background_normal: ''
    height: '40sp'

<CIBoxLayout>

<CatalogItem>:
    name: root.name if root.name else 'Restaurant Listing'
    location: root.location if root.location else 'Location'
    description: root.description if root.description else ''
    index: root.index
    orientation: 'vertical'
    selected_color: (0.2, 0.0, 0.4, 1.0)
    deselected_color: (0.1, 0.0, 0.2, 1.0)
    size_hint: (1.0, None)
    height: '80sp'
    CIBoxLayout:
        CIButton:
            background_color: root.selected_color if root.is_selected else root.deselected_color
            color: (1.0, 1.0, 0.0, 1.0) if root.is_selected else (1.0, 1.0, 1.0, 1.0)
            size_hint: (0.8, None)
            index: root.index
            font_size: '25sp'
            halign: 'left'
            text_size: self.size
            text: root.name
            bold: True if root.is_selected else False
        CIButton:
            background_color: root.selected_color if root.is_selected else root.deselected_color
            color: (1.0, 1.0, 0.0, 1.0) if root.is_selected else (1.0, 1.0, 1.0, 1.0)
            size_hint: (0.2, None)
            index: root.index
            halign: 'right'
            text_size: self.size
            text: root.location
            bold: True if root.is_selected else False
    CIBoxLayout:
        size_hint: (1.0, 1.0)
        CIButton:
            background_color: root.selected_color if root.is_selected else root.deselected_color
            color: (1.0, 1.0, 0.0, 1.0) if root.is_selected else (1.0, 1.0, 1.0, 1.0)
            size_hint: (None, None)
            index: root.index
            width: '1000sp'
            halign: 'left'
            text_size: self.size
            text: root.description
            bold: True if root.is_selected else False
            #on_press: self.deselect() if root.is_selected else self.select()

<ScreenListing>:
    name: 'OutScout Recommendations'
    ScrollView:
        scroll_timeout: 250
        scroll_distance: 20
        do_scroll_y: True
        do_scroll_x: False
        BoxLayout:
            size_hint: (1.0, 1.0)
            orientation: 'vertical'
            Button:
                size_hint_y: None
                height: 40
                text: '<-- Back to MAIN'
                on_press: app.gotoScreenMain()
            ListView:
                size_hint: (1.0, 1.0)
                id: listcontainer

<PopupRestaurant>:
    title: "Restaurant Details"
    size_hint: None,None
    #size_hint: None, 0.9
    orientation: 'vertical'
    padding: [10,50,10,50]
    spacing: 50
    width: 300
    height: 300
    auto_dismiss: True
    restaurantdescription : info
    GridLayout:
        cols:1
        Label: 
            id: info
            size_hint: None,None
            height: 40
            text: 
        Button:
            text: "Close"
            size_hint: None,None
            height: 40
            on_press: root.do_close(root)

""")

store = JsonStore('outscout.json')
accountname = ''

class CIButton(ListItemReprMixin, SelectableView, Button):
    def select(self, *args):
        if isinstance(self.parent, CatalogItem) or isinstance(self.parent, CIBoxLayout):
            self.parent.select_from_child(self, *args)

    def deselect(self, *args):
        if isinstance(self.parent, CatalogItem) or isinstance(self.parent, CIBoxLayout):
            self.parent.deselect_from_child(self, *args)


class CIBoxLayout(SelectableView, BoxLayout):
    def select(self, *args):
        if isinstance(self.parent, CatalogItem):
            self.parent.select_from_child(self, *args)

    def deselect(self, *args):
        if isinstance(self.parent, CatalogItem):
            self.parent.deselect_from_child(self, *args)

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()


class CatalogItem(SelectableView, BoxLayout):
    def select(self, *args):
        self.is_selected = True

    def deselect(self, *args):
        self.is_selected = False

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()


class DeeperListAdapter(ListAdapter):
    def create_view(self, index):
        '''Extends ListAdapter to bind events from grandchild and great
        grandchild and allow more elaborate combinations of widgets.
        '''
        #print "create_view() ", index
        item = self.get_data_item(index)
        if item is None:
            return None
        item_args = self.args_converter(index, item)
        item_args['index'] = index
        cls = self.get_cls()
        if cls:
            view_instance = cls(**item_args)
        else:
            view_instance = Builder.template(self.template, **item_args)

        if self.propagate_selection_to_data:
            # The data item must be a subclass of SelectableDataItem, or must
            # have an is_selected boolean or function, so it has is_selected
            # available. If is_selected is unavailable on the data item, an
            # exception is raised.
            if isinstance(item, SelectableDataItem):
                if item.is_selected:
                    self.handle_selection(view_instance)
            elif type(item) == dict and 'is_selected' in item:
                if item['is_selected']:
                    self.handle_selection(view_instance)
            elif hasattr(item, 'is_selected'):
                if (inspect.isfunction(item.is_selected)
                        or inspect.ismethod(item.is_selected)):
                    if item.is_selected():
                        self.handle_selection(view_instance)
                else:
                    if item.is_selected:
                        self.handle_selection(view_instance)
            else:
                msg = "ListAdapter: unselectable data item for {0}"
                raise Exception(msg.format(index))

        view_instance.bind(on_release=self.handle_selection)

        # the only actual change to this method is to bind 2 levels deeper      
        for child in view_instance.children:
            child.bind(on_release=self.handle_selection)
            for gchild in child.children:
                gchild.bind(on_release=self.handle_selection)
                for ggchild in gchild.children:
                    ggchild.bind(on_release=self.handle_selection)

        return view_instance


class ScreenMain(Screen):
    def currentAppDateTime(self):
        return (time.strftime("%A\n%B %-d %Y\n%-I:%M%p", time.localtime() ))

    
mood_dict={'All':True, 'Romantic':False, 'Formal':False, 'Casual':False}
facilities_dict={'Party':False, 'Handicap':False, 'Patio':False}
class ScreenFilter(Screen):
    def gotoScreenListing(self):
        accountname = store.get('account')['name']
        #print "store.get ", store.get('account')['name']
        if accountname == "":
            popup = LoginPopup();
            popup.open()
        else:
            sl.create_list()
            sm.transition.direction = 'left'
            sm.current = 'listing'
    def cbSetMood(self, mood, value):
        mood_dict[mood] = value
    def cbSetFacilities(self, mood, value):
        mood_dict[mood] = value


class PopupRestaurant(Popup):
    def do_close(self, popup): 
        popup.dismiss()
    def set_description(self, desc):
        self.restaurantdescription.text = desc

Restaurants = []

class ScreenListing(Screen):
    def create_list(self):
        listURL = "http://localhost/outscoutgetrestaurants.php"
        accountname = store.get('account')['name']
        #print "distance=", sf.distance.value
        #print "resturantMood=", mood_dict
        #print "resturantMood=", sf.resturantMood.value
        mood = 'All'
        for key in mood_dict.iterkeys():
            if mood_dict[key]:
                mood = key
        listInfo = { 'account' : accountname,
                     'distance' : int(sf.distance.value),
                     'mood' : mood,
                     'facilities_party' : facilities_dict['Party'],
                     'facilities_handicap' : facilities_dict['Handicap'],
                     'facilities_patio' : facilities_dict['Patio']
                     }
        r = requests.post(url = listURL, data = listInfo)
        #print(r.status_code, r.reason)
        if r.status_code == 200:
            arr = json.loads(r.text)
            #print "r.text ", r.text
            #print arr
            for i in range(0, len(arr)):
                Restaurants.append({'name': arr[i]['name'],
                                    'location': arr[i]['location'],
                                    'description': arr[i]['description'],
                                    'is_selected': False})
        self.list = DeeperListAdapter(data=Restaurants,
                                      args_converter=self.args_converter,
                                      cls=CatalogItem,
                                      selection_mode='single',
                                      allow_empty_selection=True,
                                      propagate_selection_to_data=True)
        self.list.bind(on_selection_change=self.update_selection)
        self.ids['listcontainer'].adapter = self.list

    def update_selection(self, adapter):
        #print "selection updated=", self.list.selection
        item = adapter.selection[0].text
        #print "selection item=", item
        desc = ""
        for rec in Restaurants:
            if rec['name'] == item:
                desc = rec['description']
        popup = PopupRestaurant();
        popup.set_description(desc)
        popup.open()

    def args_converter(self, row_index, an_obj):
        return {'name': an_obj['name'],
                'location': an_obj['location'],
                'description': an_obj['description']}


class LoginPopup(Popup):
    def do_login(self, popup,  accountname, password): 
        #print "user = ", accountname
        #print "pwd = ", password
        loginInfo = { 'name' : accountname, 'password': password }
        r = requests.post(url = loginURL, data = loginInfo)
        #print(r.status_code, r.reason)
        #print r.text
        if r.status_code != 200:
            self.messageLabel.text = "Login conn failed"
        elif r.text == "OK":
            #self.messageLabel.text = "Login sucess"
            store.put('account', name=accountname)
            popup.dismiss()
            sl.create_list()
            sm.transition.direction = 'left'
            sm.current = 'listing'
        else:
            self.messageLabel.text = r.text



sm = ScreenManager()
sm.add_widget(ScreenMain(name='main'))
sf = ScreenFilter(name='filter')
sm.add_widget(sf)
sl = ScreenListing(name='listing')
sm.add_widget(sl)

class MyApp(App):
    icon = '../img/coffeecup.jpeg'
    def build(self):
        self.title = 'Outscout'
        self.background_color = (0, 1, 0, 1)
        #Clock.schedule_interval(clockWidget.update, 1)
        return sm

    def gotoScreenMain(self):
        print 'app.gotoScreenMain'
        sm.transition.direction = 'right'
        sm.current = 'main'

    def gotoScreenFilter(self):
        #print 'app.gotoScreenFilter'
        sm.transition.direction = 'left'
        sm.current = 'filter'

if __name__ in ('__main__', '__android__'):
    MyApp().run()
