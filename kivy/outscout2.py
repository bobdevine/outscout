from kivy.app import App

import time
#from kivy.clock import Clock

from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
#from kivy.uix.floatlayout import FloatLayout
#from kivy.uix.anchorlayout import AnchorLayout
#from kivy.uix.scrollview import ScrollView
from kivy.uix.popup import Popup

from kivy.uix.listview import SelectableView, CompositeListItem, ListItemReprMixin
from kivy.adapters.models import SelectableDataItem
from kivy.adapters.listadapter import ListAdapter

from kivy.uix.screenmanager import ScreenManager, Screen

from kivy.config import Config
from kivy.core.window import Window

from kivy.storage.jsonstore import JsonStore

from kivy.lang import Builder
Builder.load_string("""
<LoginPopup>:
    title: "OutScout Login"
    size_hint: None,None
    #width: root.width/2
    orientation: 'vertical'
    padding: [10,50,10,50]
    spacing: 50
    width: 300
    height: 300
    auto_dismiss: False
    messageLabel : messageLabel
    txt_account : input_account
    txt_password : input_password
    GridLayout:
        cols:2
        Label: 
            size_hint: None,None
            height: 40
            text: "Email"
        TextInput:
            id: input_account
            size_hint: None,None
            height: 40
            multiline: False
        Label: 
            size_hint: None,None
            height: 40
            text: "Password"
        TextInput:
            id: input_password
            multiline: False
            password: True
            size_hint: None,None
            height: 40
        Label:
            size_hint: None,None
            height: 40
            text: ""
        Label: 
            id: messageLabel
            size_hint: None,None
            height: 40
            text: ""
        Button:
            text: "Login"
            size_hint: None,None
            height: 40
            on_press: root.do_login(root, root.txt_account.text, root.txt_password.text)

<MainScreen>:
    name: 'OutScout'
    BoxLayout:
        pos_hint: {'center_x': .5}
        orientation: 'vertical'
        align: 'center'
        Image:
            source: '../img/outscout_logo.png'
            size: (400, 400)
        Label:
            width: sp(200)
            height: sp(30)
            font_size: 50
            text: "Discover great restaurants!"
        Label:
            width: sp(200)
            height: sp(30)
            text: root.currentAppDateTime()
        Label:
            width: sp(200)
            height: sp(30)
            text: 'Our recommendation engine helps you discover great places!'
        Button:
            size_hint_y: None
            height: 40
            background_color: 0.18, .5, .92, 1
            text: 'Get Restaurant Recommendations'
            on_press: app.gotoScreenDine()

<CIButton>:
    background_normal: ''

<CIBoxLayout>

<CatalogItem>:
    name: root.name if root.name else 'ph_name'
    price: root.price if root.price else 'ph_price'
    description: root.description if root.description else 'ph_description'
    index: root.index
    orientation: 'vertical'
    selected_color: (0.2, 0.0, 0.4, 1.0)
    deselected_color: (0.1, 0.0, 0.2, 1.0)
    size_hint: (1.0, None)
    height: '80sp'
    CIBoxLayout:
        CIButton:
            background_color: root.selected_color if root.is_selected else root.deselected_color
            color: (1.0, 1.0, 0.0, 1.0) if root.is_selected else (1.0, 1.0, 1.0, 1.0)
            size_hint: (0.8, None)
            index: root.index
            height: '40sp'
            font_size: '25sp'
            halign: 'left'
            text_size: self.size
            text: root.name
            bold: True if root.is_selected else False
            #on_press: self.deselect() if root.is_selected else self.select()
        CIButton:
            background_color: root.selected_color if root.is_selected else root.deselected_color
            color: (1.0, 1.0, 0.0, 1.0) if root.is_selected else (1.0, 1.0, 1.0, 1.0)
            size_hint: (0.2, None)
            index: root.index
            height: '40sp'
            halign: 'right'
            text_size: self.size
            text: root.price
            bold: True if root.is_selected else False
            #on_press: self.deselect() if root.is_selected else self.select()
    CIBoxLayout:
        size_hint: (1.0, 1.0)
        CIButton:
            background_color: root.selected_color if root.is_selected else root.deselected_color
            color: (1.0, 1.0, 0.0, 1.0) if root.is_selected else (1.0, 1.0, 1.0, 1.0)
            size_hint: (None, None)
            index: root.index
            height: '40sp'
            width: '1000sp'
            halign: 'left'
            text_size: self.size
            text: root.description
            bold: True if root.is_selected else False
            #on_press: self.deselect() if root.is_selected else self.select()

<DineScreen>:
    name: 'OutScout Recommendations'
    ScrollView:
        scroll_timeout: 250
        scroll_distance: 20
        do_scroll_y: True
        do_scroll_x: False
        BoxLayout:
            size_hint: (1.0, 1.0)
            ListView:
                size_hint: (1.0, 1.0)
                id: listcontainer

""")

store = JsonStore('outscout.json')

accountname = 'foo'
fruit = [
    {'name': 'apple', 'price': '$0.69/lb', 'description': 'the fruit of wisdom', 'is_selected': False},
    {'name': 'pear', 'price': '$1.15/lb', 'description': 'a fine fruit that makes a fine brandy', 'is_selected': False},
    {'name': 'kiwi', 'price': '$1.00 ea', 'description': 'strange furry fruit with green pulp', 'is_selected': False},
    {'name': 'banana', 'price': '$0.44/lb', 'description': 'a long yellow fruit from a palm tree which you will enjoy if you like fruit that contains plenty of potassium', 'is_selected': False},
    {'name': 'pomegranate', 'price': '$0.44/lb', 'description': 'oh grenadine... ', 'is_selected': False},
    {'name': 'orange', 'price': '$0.44/lb', 'description': 'florida\'s best/only export', 'is_selected': True},
    {'name': 'strawberries', 'price': '$2.99/lb', 'description': 'the original red berry', 'is_selected': True},
    {'name': 'raspberries', 'price': '$3.99/lb', 'description': 'razz it up', 'is_selected': True},
    {'name': 'lemon', 'price': '$0.44/lb', 'description': 'were it not for this there would be no lemonade', 'is_selected': False}
]

class CIButton(ListItemReprMixin, SelectableView, Button):

    def select(self, *args):
        if isinstance(self.parent, CatalogItem) or isinstance(self.parent, CIBoxLayout):
            self.parent.select_from_child(self, *args)

    def deselect(self, *args):
        if isinstance(self.parent, CatalogItem) or isinstance(self.parent, CIBoxLayout):
            self.parent.deselect_from_child(self, *args)


class CIBoxLayout(SelectableView, BoxLayout):

    def select(self, *args):
        if isinstance(self.parent, CatalogItem):
            self.parent.select_from_child(self, *args)

    def deselect(self, *args):
        if isinstance(self.parent, CatalogItem):
            self.parent.deselect_from_child(self, *args)

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()


class CatalogItem(SelectableView, BoxLayout):
    
    def select(self, *args):
        self.is_selected = True

    def deselect(self, *args):
        self.is_selected = False

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()


class DeeperListAdapter(ListAdapter):

    def create_view(self, index):
        '''Extends ListAdapter to bind events from grandchild and great
        grandchild and allow more elaborate combinations of widgets.
        '''
        item = self.get_data_item(index)
        if item is None:
            return None

        item_args = self.args_converter(index, item)

        item_args['index'] = index

        cls = self.get_cls()
        if cls:
            view_instance = cls(**item_args)
        else:
            view_instance = Builder.template(self.template, **item_args)

        if self.propagate_selection_to_data:
            # The data item must be a subclass of SelectableDataItem, or must
            # have an is_selected boolean or function, so it has is_selected
            # available. If is_selected is unavailable on the data item, an
            # exception is raised.
            #
            if isinstance(item, SelectableDataItem):
                if item.is_selected:
                    self.handle_selection(view_instance)
            elif type(item) == dict and 'is_selected' in item:
                if item['is_selected']:
                    self.handle_selection(view_instance)
            elif hasattr(item, 'is_selected'):
                if (inspect.isfunction(item.is_selected)
                        or inspect.ismethod(item.is_selected)):
                    if item.is_selected():
                        self.handle_selection(view_instance)
                else:
                    if item.is_selected:
                        self.handle_selection(view_instance)
            else:
                msg = "ListAdapter: unselectable data item for {0}"
                raise Exception(msg.format(index))

        view_instance.bind(on_release=self.handle_selection)

        # the only actual change to this method is to bind 2 levels deeper
        
        for child in view_instance.children:
            child.bind(on_release=self.handle_selection)
            for gchild in child.children:
                gchild.bind(on_release=self.handle_selection)
                for ggchild in gchild.children:
                    ggchild.bind(on_release=self.handle_selection)

        return view_instance


class MainScreen(Screen):
    def currentAppDateTime(self):
        return (time.strftime("%A\n%x\n%X", time.localtime() ))

class DineScreen(Screen):
    def __init__(self, *args, **kwargs):
        self.klist = fruit

        self.catalogLA = DeeperListAdapter(data=self.klist, args_converter=self.args_converter,
                                  cls=CatalogItem, selection_mode='single',
                                  allow_empty_selection=True, propagate_selection_to_data=True)
        self.catalogLA.bind(on_selection_change=self.update_selection)
        super(self.__class__, self).__init__(*args, **kwargs)
        self.ids['listcontainer'].adapter = self.catalogLA

    def update_selection(self, adapter):
        print "selection updated=", self.catalogLA.selection
        #print fruit
    
    def args_converter(self, row_index, an_obj):
        return {'name': an_obj['name'],
                'price': an_obj['price'],
                'description': an_obj['description']}

sm = ScreenManager()
sm.add_widget(MainScreen(name='main'))
sm.add_widget(DineScreen(name='dine'))

class LoginPopup(Popup):
    def do_login(self, popup,  accountname, password): 
        #print "user = ", accountname
        #print "pwd = ", password
        if accountname == password: 
            #self.messageLabel.text = "Login sucess"
            store.put('account', name=accountname)
            popup.dismiss()
            #sm.transition = SlideTransition(direction="left")
            sm.transition.direction = 'left'
            sm.current = 'dine'
        else: 
            self.messageLabel.text = "Login failed"


class MyApp(App):
    icon = '../img/coffeecup.jpeg'
    def build(self):
        self.title = 'Outscout'
        self.background_color = (0, 1, 0, 1)
        #Clock.schedule_interval(clockWidget.update, 1)
        return sm

    def gotoScreenMain(self):
        print 'app.gotoScreenMain'
        sm.transition.direction = 'right'
        sm.current = 'main'

    def gotoScreenDine(self):
        #print 'app.gotoScreenDine'
        popup = LoginPopup();
        popup.open()

if __name__ in ('__main__', '__android__'):
    MyApp().run()
