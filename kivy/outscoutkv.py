from kivy.app import App

from kivy.lang import Builder

from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.tabbedpanel import TabbedPanelHeader


Builder.load_file("outscout.kv");


class HomeScreen(Screen):
    pass

class RestaurantScreen(Screen):
    pass

class SettingsScreen(Screen):
    pass

# Create the screen manager
sm = ScreenManager()
sm.add_widget(HomeScreen(name='home'))
sm.add_widget(RestaurantScreen(name='restaurant'))
sm.add_widget(SettingsScreen(name='settings'))

class MyApp(App):
    def build(self):
        return sm
    def do_login(self, *args):
        print'jo'

if __name__ == '__main__':
    MyApp().run()
