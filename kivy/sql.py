from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy import ForeignKey
from sqlalchemy import table
from sqlalchemy.orm import relationship, backref
from kivy.graphics import Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.graphics import Color, Rectangle
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.textinput import TextInput
from kivy.app import App
import os.path


DATABASE = 'sqlite:///db.sqlite3'
DEBUG = True


Base = declarative_base()

class MyPatient(Base):
    __tablename__ = 'patients'

    id = Column(Integer, primary_key=True)

    def __init__(self, id, ):
        self.id = id

        def __repr__(self):
                return "<Patient('%s = %s')>" % (self.id,)


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "patients.db")


engine = create_engine(DATABASE, echo=DEBUG)
session_factory = sessionmaker(bind=engine)
session = session_factory()

Base.metadata.create_all(engine)


class MyPatientWidget(ScreenManager):
    def __init__(self, patient, **kwargs):
        super(MyPatientWidget, self).__init__(**kwargs)
        self.add_widget(Label(text='Patient ID'))
        self.id = TextInput(multiline=False)
        self.add_widget(self.id)

Builder.load_string ("""
<MenuScreen>:
    AnchorLayout:
        anchor_x: 'center'
        anchor_y: 'top'
        ScreenManager:
            size_hint: 1, .9
            id: screen_manager
            Screen:
                name: 'screen1'
                BoxLayout:
                    orientation: 'vertical'
                    padding: 25
                    BoxLayout:
                        orientation: 'horizontal'
                        Label:
                            text: 'text'
                    BoxLayout:
                        orientation: 'horizontal'
                        Label:
                            text: 'id'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:

                    BoxLayout:
                        orientation: 'horizontal'
                        Label:
                            text: 'another field'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:
                    BoxLayout:
                        orientation: 'horizontal'
                        Label:
                            text: 'another field'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:
                    BoxLayout:
                        orientation: 'horizontal'
                        Label:
                            text: 'another field'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:
                    BoxLayout:
                        orientation: 'horizontal'
                        Label:
                            text: 'another field'
                        TextInput:
                        Label:
                            text: 'another field'
                        TextInput:
            Screen:
                name: 'screen2'
                Label:
                    text: 'Another Screen'


    AnchorLayout:
        anchor_x: 'center'
        anchor_y: 'bottom'
        BoxLayout:
            orientation: 'horizontal'
            size_hint: 1, .1
            Button:
                text: 'Retrieve'
            Button:
                text: 'Add'
            Button:
                text: 'Delete'
            Button:
                text: 'Clear'
""")


class MenuScreen(Screen):
   pass

class TabTextInput(TextInput):
    def __init__(self, *args, **kwargs):
        self.next = kwargs.pop('next', None)
        super(TabTextInput, self).__init__(*args, **kwargs)

    def set_next(self, next):
        self.next = next

    def _keyboard_on_key_down(self, window, keycode, text, modifiers):
        key, key_str = keycode
        if key in (9, 13) and self.next is not None:
            self.next.focus = True
            self.next.select_all()
        else:
            super(TabTextInput, self)._keyboard_on_key_down(window, keycode,    text, modifiers)


class MyApp(App):
    screen_manager = MenuScreen

    def build(self):
        self.screen_manager = ScreenManager()
        self.screen_manager.add_widget(MenuScreen(name='MyPatient Base'))
        patients = session.query(MyPatient).all()
        for r in patients:
            widget = MyPatientWidget(r)
            layout.add_widget(widget)
            layout = MenuScreen
        return self.screen_manager


if __name__ == '__main__':
    MyApp().run()
