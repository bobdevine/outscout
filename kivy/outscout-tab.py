from kivy.app import App

from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
#from kivy.uix.floatlayout import FloatLayout
#from kivy.uix.anchorlayout import AnchorLayout
#from kivy.uix.scrollview import ScrollView
from kivy.uix.popup import Popup
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.tabbedpanel import TabbedPanelHeader
from kivy.config import Config

from functools import partial

from kivy.storage.jsonstore import JsonStore

from kivy.lang import Builder
Builder.load_string("""
<LoginPopup>:
    title: "OutScout Login"
    size_hint: None,None
    #width: root.width/2
    orientation: 'vertical'
    padding: [10,50,10,50]
    spacing: 50
    width: 300
    height: 300
    auto_dismiss: False
    messageLabel : messageLabel
    txt_account : txt_account
    txt_password : txt_password
    GridLayout:
        cols:2
        Label: 
            size_hint: None,None
            height: 40
            text: "Email"
        TextInput:
            id: txt_account
            size_hint: None,None
            height: 40
            multiline: False
        Label: 
            size_hint: None,None
            height: 40
            text: "Password"
        TextInput:
            id: txt_password
            multiline: False
            password: True
            size_hint: None,None
            height: 40
        Label:
            size_hint: None,None
            height: 40
            text: ""
        Label: 
            id: messageLabel
            size_hint: None,None
            height: 40
            text: ""
        Button:
            text: "Login"
            size_hint: None,None
            height: 40
            on_press: root.do_login(root, root.txt_account.text, root.txt_password.text)

""")

store = JsonStore('outscout.json')

class LoginPopup(Popup):
    def do_login(self, popup,  accountname, password): 
        #print "user = ", accountname
        #print "pwd = ", password
        if accountname == password: 
            self.messageLabel.text = "Login sucess"
            popup.dismiss()
            store.put('account', name=accountname)
        else: 
            self.messageLabel.text = "Login failed"

class Restaurant_Page(Widget):
    def __init__(self, **kwargs):
        super(Restaurant_Page, self).__init__(**kwargs)
        layout = BoxLayout(padding=10, orientation="vertical")
        #layout = AnchorLayout(padding=10, anchor_x='center', anchor_y='top')
        if store.exists('account'):
            accountname = store.get('account')['name']
        else:
            accountname='foo'
        lbl = Label(text=accountname)
        layout.add_widget(lbl)
        layout.add_widget(TextInput(text='Hi'))

        btn = Button(text="POPUP", on_press=partial(self.show_popup))
        layout.add_widget(btn) 
        self.add_widget(layout)
        
    def show_popup(self, *args):
        popup = LoginPopup();
        popup.open()
        
class MyApp(App):
    icon = '../img/coffeecup.jpeg'
    def build(self):
        self.title = 'Outscout'
        tb_panel= TabbedPanel()
        tb_panel.do_default_tab = True
        tb_panel.default_tab_text = 'Home'
        #tb_panel.tab_width = self.parent.width / 4
        tb_panel.background_color = (0, 1, 0, 1)
        
        tb_panel.default_tab_content = Image(source='../img/outscout_logo.png',pos=(400, 100), size=(400, 400))

        th_head_dine = TabbedPanelHeader(text='DineOut')
        th_head_dine.content = Restaurant_Page()

        th_head4 = TabbedPanelHeader(text='Settings')
        th_head4.content = Button(text='This is my button', font_size=20)
        
        tb_panel.add_widget(th_head_dine)
        tb_panel.add_widget(th_head4)          
 
        return tb_panel
    def do_login(self, *args):
        print'jo'

if __name__ == '__main__':
    MyApp().run()
